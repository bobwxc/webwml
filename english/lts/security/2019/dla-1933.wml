<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A command injection vulnerability in Nokogiri allows commands to be executed in
a subprocess by Ruby's `Kernel.open` method.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.6.3.1+ds-1+deb8u1.</p>

<p>We recommend that you upgrade your ruby-nokogiri packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1933.data"
# $Id: $
