<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that XStream is vulnerable to Remote Code Execution. The
vulnerability may allow a remote attacker to run arbitrary shell commands only
by manipulating the processed input stream. Users who rely on blocklists
are affected (the default in Debian). We strongly recommend to use the
whitelist approach of XStream's Security Framework because there are likely
more class combinations the blacklist approach may not address.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.4.9-2+deb9u1.</p>

<p>We recommend that you upgrade your libxstream-java packages.</p>

<p>For the detailed security status of libxstream-java please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libxstream-java">https://security-tracker.debian.org/tracker/libxstream-java</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2471.data"
# $Id: $
