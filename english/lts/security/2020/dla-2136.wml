<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an out-of-bounds buffer read vulnerability
in <tt>libvpx</tt>, a library implementing the VP8 &amp; VP9 video codecs.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-0034">CVE-2020-0034</a></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.3.0-3+deb8u3.</p>

<p>We recommend that you upgrade your libvpx packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2136.data"
# $Id: $
