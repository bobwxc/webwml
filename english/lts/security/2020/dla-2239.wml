<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an issue in libpam-tacplus (a security
module for using the TACACS+ authentication service) where shared secrets such
as private server keys were being added in the clear to various logs.</p>

<ul>

	<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13881">CVE-2020-13881</a>

	<p>It was discovered that there was an issue in libpam-tacplus (a security
	module for using the TACACS+ authentication service) where shared secrets such
	as private server keys were being added in the clear to various logs.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.3.8-2+deb8u1.</p>

<p>We recommend that you upgrade your libpam-tacplus packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2239.data"
# $Id: $
