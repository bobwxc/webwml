<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were discovered in Enigmail, an OpenPGP
extension for Thunderbird, which could result in a loss of
confidentiality, faked signatures, plain text leaks and denial of
service. Additional information can be found under
<a href="https://enigmail.net/download/other/Enigmail%20Pentest%20Report%20by%20Cure53%20-%20Excerpt.pdf">https://enigmail.net/download/other/Enigmail%20Pentest%20Report%20by%20Cure53%20-%20Excerpt.pdf</a></p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2:1.9.9-1~deb7u1.</p>

<p>We recommend that you upgrade your enigmail packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1219.data"
# $Id: $
