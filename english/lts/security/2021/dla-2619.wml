<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Three security issues have been discovered in python3.5:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3177">CVE-2021-3177</a>

    <p>Python 3.x  has a buffer overflow in PyCArg_repr in _ctypes/callproc.c,
    which may lead to remote code execution in certain Python applications that accept
    floating-point numbers as untrusted input.
    This occurs because sprintf is used unsafely.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3426">CVE-2021-3426</a>

    <p>Running `pydoc -p` allows other local users to extract arbitrary files.
    The `/getfile?key=path` URL allows to read arbitrary file on the filesystem.</p>

    <p>The fix removes the <q>getfile</q> feature of the pydoc module which
    could be abused to read arbitrary files on the disk (directory
    traversal vulnerability).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23336">CVE-2021-23336</a>

    <p>The Python3.5 vulnerable to Web Cache Poisoning via urllib.parse.parse_qsl
    and urllib.parse.parse_qs by using a vector called parameter cloaking. When
    the attacker can separate query parameters using a semicolon (;), they can
    cause a difference in the interpretation of the request between the proxy
    (running with default configuration) and the server. This can result in malicious
    requests being cached as completely safe ones, as the proxy would usually not
    see the semicolon as a separator, and therefore would not include it in a cache
    key of an unkeyed parameter.</p>

    <p><b>Attention, API-change!</b> </p>

    <p> Please be sure your software is working properly if it uses `urllib.parse.parse_qs`
    or `urllib.parse.parse_qsl`, `cgi.parse` or `cgi.parse_multipart`.</p>

    <p>Earlier Python versions allowed using both  ``;`` and ``&`` as query parameter
    separators in `urllib.parse.parse_qs` and `urllib.parse.parse_qsl`.
    Due to security concerns, and to conform with
    newer W3C recommendations, this has been changed to allow only a single
    separator key, with ``&`` as the default.  This change also affects
    `cgi.parse` and `cgi.parse_multipart` as they use the affected
    functions internally. For more details, please see their respective
    documentation.</p>


<p>For Debian 9 stretch, these problems have been fixed in version
3.5.3-1+deb9u4.</p>

<p>We recommend that you upgrade your python3.5 packages.</p>

<p>For the detailed security status of python3.5 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/python3.5">https://security-tracker.debian.org/tracker/python3.5</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2619.data"
# $Id: $
