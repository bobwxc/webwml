<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been found in the Apache HTTPD server.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9517">CVE-2019-9517</a>

    <p>Jonathan Looney reported that a malicious client could perform a
    denial of service attack (exhausting h2 workers) by flooding a
    connection with requests and basically never reading responses on
    the TCP connection.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10081">CVE-2019-10081</a>

    <p>Craig Young reported that HTTP/2 PUSHes could lead to an overwrite
    of memory in the pushing request's pool, leading to crashes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10082">CVE-2019-10082</a>

    <p>Craig Young reported that the HTTP/2 session handling could be made
    to read memory after being freed, during connection shutdown.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10092">CVE-2019-10092</a>

    <p>Matei <q>Mal</q> Badanoiu reported a limited cross-site scripting
    vulnerability in the mod_proxy error page.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10097">CVE-2019-10097</a>

    <p>Daniel McCarney reported that when mod_remoteip was configured to
    use a trusted intermediary proxy server using the <q>PROXY</q> protocol,
    a specially crafted PROXY header could trigger a stack buffer
    overflow or NULL pointer deference. This vulnerability could only be
    triggered by a trusted proxy and not by untrusted HTTP clients. The
    issue does not affect the stretch release.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10098">CVE-2019-10098</a>

    <p>Yukitsugu Sasaki reported a potential open redirect vulnerability in
    the mod_rewrite module.</p></li>

</ul>

<p>For the oldstable distribution (stretch), these problems have been fixed
in version 2.4.25-3+deb9u8.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 2.4.38-3+deb10u1.</p>

<p>We recommend that you upgrade your apache2 packages.</p>

<p>For the detailed security status of apache2 please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/apache2">https://security-tracker.debian.org/tracker/apache2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4509.data"
# $Id: $
