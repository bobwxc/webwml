#use wml::debian::template title="FAQ de auditoria de segurança do Debian"
#use wml::debian::toc
#use wml::debian::translation-check translation="b8114b588961778dbd04974c1464a2f388a90c28"

<p>Esta página lista algumas das questões mais comuns que
visitantes podem ter ao ouvir pela primeira vez sobre este projeto.</p>

<toc-display>

<toc-add-entry name="what">O que é o projeto de auditoria de segurança do
Debian?</toc-add-entry>

<p>O projeto de auditoria de segurança do Debian é um pequeno projeto conduzido
dentro do projeto Debian, desenhado para ter uma atitude proativa frente
à segurança pela realização de auditorias de código-fonte dos pacotes
disponíveis aos(às) usuários(as) do Debian.</p>

<p>A auditoria é focada na distribuição estável (stable) do Debian, com o
trabalho de auditoria sendo orientado pelas <a href="packages">diretrizes
de priorização de pacotes</a>.</p>

<toc-add-entry name="start">Quando o projeto de auditoria de segurança do
Debian foi iniciado?</toc-add-entry>

<p>O primeiro aviso foi lançado em dezembro de 2002, seguido por uma
série de avisos adicionais posteriormente.</p>

<p>Ele continuou como uma função não oficial até ter sido concedido seu
status <q>oficial</q> em maio de 2004 pelo líder do projeto Debian, Martin
Michlmayr.</p>

<toc-add-entry name="advisories-from-audit">Que avisos foram
resultado de esforços de auditoria?</toc-add-entry>

<p>Múltiplos avisos foram lançados como parte do trabalho de
auditoria, e todos aqueles que foram lançados antes do projeto ter
recebido seu status oficial estão listados na <a href="advisories">página
de avisos de auditoria</a>.</p>

<p>Espera-se que em um futuro próximo, os avisos publicamente conhecidos do
projeto possam ser encontrados ao se ler os relatórios
de avisos de segurança do Debian e ao se procurar pelo <q>projeto de
auditoria de segurança do Debian</q>.</p>

<toc-add-entry name="advisories">Todo o trabalho de auditoria está relacionado
com avisos?</toc-add-entry>

<p>Na verdade, não. Existem muitos problemas de segurança que o processo
de auditoria descobriu que não são imediatamente exploráveis (eles podem,
contudo, fazer com que o programa quebre). Outros problemas de segurança que
podem ser explorados, e que nós descobrimos, não estavam presentes na versão
estável (stable) oficial do Debian, mas estavam presentes na versão teste
(testing) e instável (unstable).
Todos foram relatados através do sistema de rastreamento de bugs do Debian
(e em alguns casos, diretamente aos(às) autores(as)).</p>


<toc-add-entry name="credit">Quem contribuiu para este trabalho?</toc-add-entry>

<p>Steve Kemp começou o projeto de auditoria de segurança do Debian, criando
o processo inicial e testando-o ao encontrar muitas vulnerabilidades.</p>

<p>Ulf Härnhammar juntou-se ao projeto durante este período não oficial inicial
e encontrou diversas vulnerabilidades que foram logo corrigidas; Ulf foi
logo seguido por Swaraj Bontula e Javier Fern&aacute;ndez-Sanguino, que
também encontraram muitos e significantes problemas de segurança.</p>

<p><a href="http://www.dwheeler.com">David A. Wheeler</a> instigou Steve
Kemp a se voluntariar para liderar o projeto como um projeto oficial do
Debian, o que foi possível pelo envolvimento do líder do projeto Debian,
Martin Michlmayr. O David também fez várias sugestões úteis sobre o conteúdo
dessas páginas, diretamente contribuindo para várias seções.</p>

<p>O <a href="$(HOME)/security">time de segurança do
Debian</a> foi muito prestativo em fazer das auditorias um sucesso,
garantindo que qualquer vulnerabilidade encontrada fosse rapidamente
corrigida e distribuída para o mundo.</p>

<p>As seguintes pessoas contribuíram com ao menos um aviso de segurança
em nome do projeto:</p>

#include "$(ENGLISHDIR)/security/audit/data/credits.inc"

<p>Mais contribuidores(as) são sempre bem-vindos(as)!</p>


<toc-add-entry name="contribute">Como eu posso contribuir?</toc-add-entry>

<p>Se você tem tempo e habilidades necessários para auditar um pacote, então
simplesmente vá em frente e faça!</p>

<p>A <a href="auditing">visão geral sobre auditoria</a> deve te oferecer uma
boa ideia de como realizar o trabalho &mdash; qualquer dúvida adicional que
você possa ter pode ser perguntada (em inglês) na <a
href="https://lists.debian.org/debian-security/">lista de discussão
debian-security</a>.</p>

# A FAZER - Lista antiga, não mais disponível
# Perguntar ao Steve Kemp para fornecer os arquivos e movê-los para uma lista de
# discussão oficial?
#<a
#href="http://shellcode.org/mailman/listinfo/debian-audit">lista de discussão
#debian-audit</a>.</p>

<toc-add-entry name="mailinglist">Eu posso discutir sobre pacotes específicos na lista de discussão?</toc-add-entry>

<p>O melhor é que você não nomeie os pacotes contendo problemas que você
descobriu antes de uma <a href="$(HOME)/security/">DSA</a> ter sido
lançada. Já que isto permite que usuários(as) maliciosos(as) se aproveitem
de qualquer falha que você descrever antes delas serem corrigidas.</p>

<p>Em vez disso, a lista de discussão pode ser usada para descrever uma
parte do código e para perguntar as opiniões se ele é explorável, e como
pode ser corrigido.</p>

<toc-add-entry name="maintainer">Como eu posso contribuir sendo um(a)
mantenedor(a) de pacote?</toc-add-entry>

<p>Os(as) mantenedores(as) de pacotes podem ajudar garantindo a segurança
do software que eles(as) empacotam ao examinar o códigos eles(as) mesmos(as),
ou pedindo por ajuda.</p>

<p>Por favor veja a síntese sobre <a href="maintainers">auditoria para
mantenedores(as) de pacotes</a>.</p>

<toc-add-entry name="reporting">Como eu reporto um problema que eu
descobri?</toc-add-entry>

<p>Existe uma seção no <a href="$(HOME)/security/faq#discover">FAQ do
time de segurança</a> descrevendo o processo.</p>

<toc-add-entry name="clean">Os pacotes auditados e considerados limpos estão
disponíveis?</toc-add-entry>

<p>Não, os pacotes que foram examinados e não tiveram problemas encontrados
dentro deles não são listados publicamente.</p>

<p>Isto acontece, em parte, porque pode haver problemas escondidos
que passaram desapercebidos, e em parte porque as auditorias são conduzidas
sem um grau muito grande de coordenação por muitas pessoas.</p>

<toc-add-entry name="moreinfo">Onde eu posso encontrar mais
informações?</toc-add-entry>

<p>Atualmente não existe uma lista de discussão na qual você possa se inscrever
para fazer perguntas. Enquanto isso, por favor use a
<a
href="https://lists.debian.org/debian-security/">lista de discussão
debian-security</a>.</p>
