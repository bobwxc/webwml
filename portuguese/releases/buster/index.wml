#use wml::debian::template title="Informações de lançamento do Debian &ldquo;buster&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/buster/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="6cb92bf51af80b4a4dc15d92247adf5ba8fe9619"

<if-stable-release release="buster">

<p>O Debian <current_release_buster> foi lançado em
<a href="$(HOME)/News/<current_release_newsurl_buster/>"><current_release_date_buster></a>.
<ifneq "10.0" "<current_release>"
  "O Debian 10.0 foi inicialmente lançado em <:=spokendate('2019-07-06'):>."
/>
O lançamento incluiu muitas mudanças importantes, descritas no
nosso <a href="$(HOME)/News/2019/20190706">comunicado à imprensa</a> e
nas <a href="releasenotes">notas de lançamento</a>.</p>

#<p><strong>O Debian 10 foi substituído pelo
#<a href="../lenny/">Debian 11 (<q>bullseye</q>)</a>.
#Atualizações de segurança foram descontinuadas em <:=spokendate('xxxx-xx-xx'):>.
#</strong></p>

### Este parágrafo é orientativo, revise antes de publicar!
#<p><strong>Contudo, o buster se beneficia do suporte de longo prazo (LTS -
#Long Term Support) até o final de xxxxx 20xx. O LTS é limitado ao i386, amd64,
#armel, armhf e arm64.
#Todas as outras arquiteturas não são mais suportadas no buster.
#Para mais informações, consulte a <a
#href="https://wiki.debian.org/LTS">seção LTS da wiki do Debian</a>.
#</strong></p>

<p>Para obter e instalar o Debian, veja a página <a
href="debian-installer/">informações de instalação</a> e o
<a href="installmanual">Guia de Instalação</a>. Para atualizar a partir de uma
versão mais antiga do Debian, veja as instruções nas
<a href="releasenotes">notas de lançamento</a>.</p>

###  Ative o seguinte, quando o período LTS começar.
#<p>Arquiteturas suportadas durante o suporte de longo prazo:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>As seguintes arquiteturas de computadores são suportadas nesta versão:</p>
# <p>Computer architectures supported at initial release of buster:</p> ### Use this line when LTS starts, instead of the one above.

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Ao contrário do que desejamos, pode haver alguns problemas existentes na
versão, mesmo que ela seja declarada <em>estável (stable)</em>. Nós fizemos
<a href="errata">uma lista dos principais problemas conhecidos</a>, e
você sempre pode nos
<a href="reportingbugs">relatar outros problemas</a>.</p>

<p>Por último, mas não menos importante, nós temos uma lista de <a
href="credits">pessoas que merecem crédito</a> por fazer este
lançamento acontecer.</p>
</if-stable-release>

<if-stable-release release="stretch">

<p>O codinome da próxima versão principal do Debian após a <a
href="../stretch/">stretch</a> é <q>buster</q>.</p>

<p>Essa versão começou como uma cópia da stretch e está atualmente em
um estado chamado <q><a
href="$(DOC)/manuals/debian-faq/ftparchives#testing">teste (testing)</a></q>.
Isso significa que as coisas não devem quebrar de maneira tão ruim quanto
nas versões instável (unstable) ou experimental, porque os pacotes só entram
nesta versão após um certo tempo e quando eles não têm bugs críticos
ao lançamento registrados contra eles.</p>

<p>Por favor, tenha em mente que atualizações de segurança para a versão
teste (testing) <strong>não</strong> são mantidos ainda pelo time de segurança.
Por isso, a <q>testing</q>
<strong>não</strong> recebe atualizações de segurança de maneira oportuna.
# For more information please see the
# <a href="https://lists.debian.org/debian-testing-security-announce/2008/12/msg00019.html">announcement</a>
# of the Testing Security Team.
Considere-se encorajado a trocar as suas entradas
sources.list da testing para a stretch por enquanto se precisar de
suporte de segurança. Veja também a entrada no
<a href="$(HOME)/security/faq#testing">FAQ do Time de Segurança</a>
para a versão teste (testing).</p>

<p>Pode ser que já exista um <a href="releasenotes">esboço das notas
de lançamento disponível</a>. Por favor também <a
href="https://bugs.debian.org/release-notes">cheque as adições
propostas às notas de lançamento</a>.</p>

<p>Para imagens de instalação e documentação sobre como instalar a <q>testing</q>,
veja <a href="$(HOME)/devel/debian-installer/">a página do Debian-Installer</a>.</p>

<p>Para descobrir mais a respeito de como a versão teste (testing( funciona,
veja <a href="$(HOME)/devel/testing">as informações dos(as) desenvolvedores(as)
a respeito dela</a>.</p>

<p>As pessoas frequentemente perguntam se já um <q>medidor de
progresso</q> do lançamento. Infelizmente não existe, mas nós
podemos lhe encaminhar a vários lugares que descrevem os fatores que
precisam ser resolvidos para o lançamento acontecer:</p>

<ul>
  <li><a href="https://release.debian.org/">Página genérica do status
  do lançamento</a></li>
  <li><a href="https://bugs.debian.org/release-critical/">Bugs
  críticos ao lançamento</a></li>
  <li><a
  href="https://udd.debian.org/bugs.cgi?base=only&amp;rc=1">Bugs do
  sistema básico</a></li>
  <li><a
  href="https://udd.debian.org/bugs.cgi?standard=only&amp;rc=1">Bugs
  nos pacotes padrão e task</a></li>
</ul>

<p>Além disso, relatórios gerais de status são postados pelo gerente de
lançamento na <a href="https://lists.debian.org/debian-devel-announce/">\
lista de discussão debian-devel-announce</a>.</p>

</if-stable-release>
