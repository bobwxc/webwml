#use wml::debian::template title="Debian GNU/NetBSD -- Waarom?" BARETITLE="yes" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/netbsd/menu.inc"
#use wml::debian::translation-check translation="a1cc1a182e1feb2d026ca05e6cc8963b3964e136"

<h1>Waarom Debian GNU/NetBSD?</h1>

<ul>
<li>NetBSD werkt op hardware welke niet ondersteund wordt door Linux. Debian
geschikt maken voor de NetBSD-kernel verhoogt het aantal platforms waarop het
gebruik van een op Debian gebaseerd besturingssysteem mogelijk is.</li>

<li>Het project Debian GNU/Hurd toont aan dat Debian niet gebonden is aan
één specifieke kernel. De Hurd-kernel is echter nog steeds relatief
onaf - een Debian GNU/NetBSD-systeem zou bruikbaar zijn op
productieniveau.</li>

<li>Lessen die getrokken werden uit het overzetten van Debian naar NetBSD
kunnen worden gebruikt voor het geschikt maken van Debian voor andere kernels
(zoals die van <a
href="https://www.freebsd.org/">FreeBSD</a> en <a
href="http://www.openbsd.org/">OpenBSD</a>).</li>

<li>In tegenstelling tot projecten zoals
<a href="http://www.finkproject.org/">Fink</a>
of <a href="http://debian-cygwin.sf.net/">Debian GNU/w32</a>,
is Debian GNU/NetBSD niet bedoeld om extra software of een Unix-achtige
omgeving te voorzien voor een bestaand besturingssysteem (het aantal
overzettingen van Debian naar *BSD is al uitgebreid, en deze bieden
ontegensprekelijk een Unix-achtige omgeving). In plaats daarvan zou een
gebruiker of beheerder die gewend is aan een meer traditioneel Debian-systeem
zich onmiddellijk op zijn gemak moeten voelen met een Debian GNU/NetBSD-systeem
en er zich in een relatief kort tijdsbestek bekwaam op moeten voelen.</li>

<li>Niet iedereen houdt van de *BSD-reeks omzettingen van Debian of van de
*BSD-gebruikersruimte (dit is eerder een persoonlijke voorkeur dan enige vorm
van commentaar op de kwaliteit ervan). Er zijn Linux-distributies gemaakt die
een overzetting in *BSD-stijl bieden of een gebruikersruimte in *BSD-stijl voor
degenen die van de BSD-gebruikersomgeving houden, maar tevens de Linux-kernel
willen gebruiken - Debian GNU/NetBSD is het logische omgekeerde hiervan,
waardoor mensen die van de GNU-gebruikersruimte houden of van een pakketsysteem
in Linux-stijl, de mogelijkheid krijgen om de NetBSD-kernel te gebruiken.</li>

<li>Omdat we het kunnen.</li>
</ul>

## Local variables:
## sgml-default-doctype-name: "HTML"
## End:
