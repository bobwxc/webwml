#use wml::debian::template title="Grunde til at vælge Debian" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="7141cf5f25b03b1aa3c02c1afe2a5ca8cab2f672"

<p>Der er mange årsager til hvorfor brugerne vælger Debian som deres 
styresystem.</p>

<h1>Hovedårsager</h1>

# try to list reasons for end users first

<dl>
  <dt><strong>Debian er fri software.</strong></dt>
  <dd>
    Debian består af fri og open source-software, og vil altid være 100 procent
    <a href="free">fri</a>.  Fri for alle til at anvende, ændre og distribuere. 
    Det er vores primære løfte til <a href="../users">vores brugere</a>.  Det er 
    også omkostningsfrit.
  </dd>
</dl>

<dl>
  <dt><strong>Debian er et stabilt og sikkert Linux-baseret styresystem.</strong></dt>
  <dd>
    Debian er et styresystem for et bredt udvalg af enheder, herunder bærbare og 
    stationære computere, og servere.  Siden 1993 har brugerne været glade for 
    stabiliteten og pålideligheden.  Vi leverer fornuftige standardindstillinger 
    for enhver pakke.  Debian-udviklerne stiller sikkerhedsopdateringer til 
    rådighed for alle pakker i løbet af deres levetid, hvor det er muligt.
  </dd>
</dl>

<dl>
  <dt><strong>Debian har omfattende understøttelse af hardware.</strong></dt>
  <dd>
    Det meste hardware er allerede understøttet af Linux-kernen.  Proprietære 
    drivere til hardware er tilgængelig når den frie software ikke er 
    tilstrækkelig.
  </dd>
</dl>

<dl>
  <dt><strong>Debian giver problemfrie opgraderinger.</strong></dt>
  <dd>
    Debian er velkendt for sine problemfrie opgraderinger indenfor en 
    udgivelsescyklus, men også til den næste større udgave.
  </dd>
</dl>

<dl>
  <dt><strong>Debian er udspring og grundlag for mange andre distributioner.</strong></dt>
  <dd>
    Mange af de mest populære Linux-distributioner, så som Ubuntu, Knoppix, 
    PureOS, SteamOS og Tails, har valgt Debian som grundlag for deres software.
    Debian stiller alle værktøjer til rådighed, så alle kan supplere 
    softwarepakkerne fra Debians arkiv med deres egne pakker, som opfylder deres 
    behov.
  </dd>
</dl>

<dl>
  <dt><strong>Debian-projektet er et fællesskab.</strong></dt>
  <dd>
    Debian er ikke kun et Linux-styresystem.  Softwaren fremstilles i samarbejde 
    med hundredvis af frivillige over hele verden.  Du kan deltage i Debians 
    fællesskab, selv om du ikke er programmør eller systemadministrator.  Debian 
    styres af sit fællesskab og af konsenss, og har en 
    <a href="../devel/constitution">demokratisk styreform</a>.  Da alle 
    Debian-udviklere har de samme rettigheder kan projektet ikke kontrolleres 
    af en enkelt virksomhed.  Vi har udviklere i flere end 60 lande, og vores 
    installeringsprogram, Debian Installer, er oversat til flere end 80 sprog.
  </dd>
</dl>

<dl>
  <dt><strong>Debian har adskillige installeringsmetoder.</strong></dt>
  <dd>
    Slutbrugerne kan anvende vores 
    <a href="https://cdimage.debian.org/images/release/current-live/amd64/iso-hybrid/">Live-CD</a>,
    herunder den letanvendelige Calamares-installer, som ikke behøver ret mange 
    indtastninger eller forkundskaber.  Mere erfarne brugere kan anvendes vores 
    unikke, komplette installeringsprogram, mens eksperter kan finindstille 
    installeringen eller endda anvende et automatiseret værktøj til 
    netværksinstallering.
  </dd>
</dl>

<br>

<h1>Forretningsmiljøer</h1>

<p>
  Hvis du har behov for Debian i et professionelt miljø, er der yderligere disse 
  fordele:
</p>

<dl>
  <dt><strong>Debian er pålidelig.</strong></dt>
  <dd>
    Debian beviser dagligt sin pålidelighed i tusindvis af scenarier i den 
    virkelige verden, spændende fra enkeltbrugeres bærbare computere til 
    super-collidere, børser og bilindustrien.  Det er også populært i den 
    akademiske verden, i videnskabelige miljøer og i den offentlige sektor.
  </dd>
</dl>

<dl>
  <dt><strong>Debian har mange eksperter.</strong></dt>
  <dd>
    Vores pakkevedligeholdere tager sig ikke kun af Debian-pakning og 
    indarbejdelse af nye opstrømsversioner.  Ofte er de eksperter i 
    opstrømssoftwaren, og bidrager direkte til opstrømsudviklingen.  Nogle gange 
    er de også en del af opstrøm.
  </dd>
</dl>

<dl>
  <dt><strong>Debian er sikker.</strong></dt>
  <dd>
    Debian har sikkerhedsunderstøttelse af sine stabile udgivelser.  Mange andre 
    distributioner og sikkerhedsefterforskere er afhængige af Debians 
    sikkerhedssporingsværktøj.
  </dd>
</dl>

<dl>
  <dt><strong>Long Term Support.</strong></dt>
  <dd>
    Der er gratis <a href="https://wiki.debian.org/LTS">Long Term Support</a>
    (LTS, langtidsunderstøttelse).  Det giver udvidet understøttelse af den 
    stabile udgave i fem år eller mere.  Desuden er der initiativet
    <a href="https://wiki.debian.org/LTS/Extended">Extended LTS</a>, som 
    udvider understøttelse af et begrænset antal pakker i mere end fem år.
  </dd>
</dl>

<dl>
  <dt><strong>Cloud-filaftryk.</strong></dt>
  <dd>
    Officielle cloud-filaftryk er tilgængelige hos alle de største platforme.  
    Vi tilbyder også værktøjer og opsætninger, så du kan opbygge dit eget 
    skræddersyede cloud-filaftryk.  Du kan også anvende Debian i virtuelle 
    maskiner på desktop'en eller i en container.
  </dd>
</dl>

<br>

<h1>Udviklere</h1>

<p>Debian er vidt udbredt blandt alle former for software- og hardwareudviklere.</p>

<dl>
  <dt><strong>Offentligt tilgængeligt fejlsporingssystem.</strong></dt>
  <dd>
    Vores <a href="../Bugs">fejlsporingssystem</a> (BTS) er offentligt 
    tilgængeligt for alle gennem en webbrowser.  Vi skjuler ikke vores 
    softwarefejl, og du kan let indsende nye fejlrapporter.
  </dd>
</dl>

<dl>
  <dt><strong>IoT og indlejrede enheder.</strong></dt>
  <dd>
    Vi understøtter et bredt udvalg af enheder, som eksempelvis Raspberry Pi, 
    forskellige varianter af QNAP, mobile enheder, hjemmerouter og mange 
    Single Board Computers (SBC).
  </dd>
</dl>

<dl>
  <dt><strong>Mange hardwarearkitekturer.</strong></dt>
  <dd>
    Der er understøttelse af en <a href="../ports">lang liste</a> af 
    CPU-arkitekturer, herunder amd64, i386, mange versioner af ARM og MIPS, 
    POWER7, POWER8, IBM System z og RISC-V.  Debian er også tilgængelig for 
    ældre og specifikke nichearkitekturer.
  </dd>
</dl>

<dl>
  <dt><strong>Enormt antal tilgængelige softwarepakker.</strong></dt>
  <dd>
    Debian har det største antal af pakker tilgængelige for installering
    (i øjeblikket <packages_in_stable>).  Vores pakker anvender deb-formatet, 
    som er velkendt for sin høje kvalitet.
  </dd>
</dl>

<dl>
  <dt><strong>Forskellige tilgængelige udgivelser.</strong></dt>
  <dd>
    Ud over vores stabile udgave, kan du også få de nyeste softwareversioner ved 
    at anvende udgaverne testing eller unstable.
  </dd>
</dl>

<dl>
  <dt><strong>Høj kvalitet ved hjælp af udviklerværktøjer og fremgangsmåder.</strong></dt>
  <dd>
    Flere udviklerværktøjer hjælper med at holde kvaliteten på et højt niveau, 
    og vores <a href="../doc/debian-policy/">fremgangsmåder</a> definerer de 
    tekniske krav, som alle pakker skal opfylde for at kunne blive medtaget i 
    distributionen.  Vores continuous integration kører softwaren autopkgtest, 
    piuparts er vores testværktøj til installering, opgradering og fjernelse, 
    og lintian er et omfattende værktøj til at kontrollere Debian-pakker med.
  </dd>
</dl>

<br>

<h1>Hvad vores brugere siger</h1>

<ul style="line-height: 3em">
  <li>
    <q><strong>
      For mig er der et perfekt niveau af brugervenlighed og stabilitet.  Jeg 
      har brugt forskellige distributioner i årenes løb, men Debian er den 
      eneste som bare virker.
    </strong></q>
  </li>

  <li>
    <q><strong>
      Klippestabil.  Massevis af pakker.  Fremragende fællesskab.
    </strong></q>
  </li>

  <li>
    <q><strong>
      For mig er Debian symbolet på stabilitet og brugervenlighed.
    </strong></q>
  </li>
</ul>
