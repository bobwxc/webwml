#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Une vulnérabilité a été découverte dans libapache-mod-jk, le connecteur
d’Apache 2 pour Tomcat, le moteur de servlet Java.</p>

<p>Le connecteur libapache-mod-jk est susceptible à une divulgation
d'informations et augmentation de droits à cause d’un mauvais traitement de
normalisation d’URL.</p>

<p>La nature du correctif demande que libapache-mod-jk dans Debian 8
<q>Jessie</q> soit mis à jour vers la dernière publication amont. Pour
référence, les modifications de l’amont en fonction de chaque publication sont
documentées ici :</p>

<p><url "http://tomcat.apache.org/connectors-doc/miscellaneous/changelog.html"></p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans la version 1.2.46-0+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libapache-mod-jk.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1609.data"
# $Id: $
