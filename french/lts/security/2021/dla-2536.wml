#use wml::debian::translation-check translation="08b267f4e02af2292ea2669bade33cfe5ca3eb59" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs problèmes ont été découverts dans libsdl2, une bibliothèque pour un
accès de bas niveau au tampon de trame, à la sortie audio, à la souris et au
clavier. Tous les problèmes concernent soit un dépassement de tampon, un
dépassement d'entier ou une lecture hors limites de tampon de tas, aboutissant
à un déni de service ou une exécution de code à distance en utilisant des
fichiers contrefaits de différents formats.</p>


<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 2.0.5+dfsg1-2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libsdl2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libsdl2, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libsdl2">https://security-tracker.debian.org/tracker/libsdl2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2536.data"
# $Id: $
