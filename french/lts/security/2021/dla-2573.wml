#use wml::debian::translation-check translation="1af01e1159c47d0b9f2c0aed60415115e6e721cd" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été découvert que zstd, un utilitaire de compression, était vulnérable
à une situation de compétition : il expose temporairement, durant un très court
laps de temps, une version lisible par tout le monde de son entrée même si le
fichier originel avait des permissions restreintes.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 1.1.2-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libzstd.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libzstd, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libzstd">\
https://security-tracker.debian.org/tracker/libzstd</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2573.data"
# $Id: $
