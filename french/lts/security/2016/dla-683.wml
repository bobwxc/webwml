#use wml::debian::translation-check translation="55d1ac616e9ec6fe42ad1680e45c2ce133b85547" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le paquet
graphicsmagick qui pourraient conduire à un déni de service au moyen
d'échecs d'assertion et d'utilisation de processeur ou de mémoire.
Certaines vulnérabilités peuvent aussi mener à l'exécution de code mais
aucune exploitation n'est connue à ce jour.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7448">CVE-2016-7448</a>

<p>Utah RLE : rejet de fichiers tronqués ou aberrants qui provoquent des
allocations énormes de mémoire ou une consommation excessive de processeur </p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7996">CVE-2016-7996</a>

<p>absence de vérification que la table de correspondance de couleur
fournie ne contenait pas plus de 256 entrées avec pour conséquence un
potentiel dépassement de tas</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7997">CVE-2016-7997</a>

<p>déni de service à l'aide d'un plantage dû à une assertion</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8682">CVE-2016-8682</a>

<p>dépassement de pile dans ReadSCTImage (sct.c)</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8683">CVE-2016-8683</a>

<p>échec d'allocation de mémoire dans ReadPCXImage (pcx.c)</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8684">CVE-2016-8684</a>

<p>échec d'allocation de mémoire dans MagickMalloc (memory.c)</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 1.3.16-1.1+deb7u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets graphicsmagick.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-683.data"
# $Id: $
