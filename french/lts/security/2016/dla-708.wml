#use wml::debian::translation-check translation="55d1ac616e9ec6fe42ad1680e45c2ce133b85547" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été découverts dans le serveur de base de
données MySQL. Les vulnérabilités sont corrigées en mettant MySQL à niveau
vers la nouvelle version amont 5.5.53, qui comprend d'autres changements
tels que des améliorations de performance, des corrections de bogues, de
nouvelles fonctionnalités et éventuellement des modifications
incompatibles. Veuillez consulter les notes de publication de MySQL 5.5 et
les annonces de mises à jour critiques d'Oracle pour de plus amples
détails :</p>

<ul>
 <li><a href="https://dev.mysql.com/doc/relnotes/mysql/5.5/en/news-5-5-53.html">\
https://dev.mysql.com/doc/relnotes/mysql/5.5/en/news-5-5-53.html</a></li>
 <li><a href="http://www.oracle.com/technetwork/security-advisory/cpuoct2016-2881722.html">\
http://www.oracle.com/technetwork/security-advisory/cpuoct2016-2881722.html</a></li>
</ul>

<p>Veuillez noter aussi que le paquet créera désormais
/var/lib/mysql-files, dans la mesure où le serveur restreindra maintenant
toutes les opérations d'import/export par défaut vers ce répertoire. Cela
peut être modifié avec l'option de configuration secure-file-priv.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 5.5.53-0+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mysql-5.5.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-708.data"
# $Id: $
