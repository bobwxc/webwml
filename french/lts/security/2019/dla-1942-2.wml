#use wml::debian::translation-check translation="5de6f08afb5b48247951a38f158e8bcd87190465" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il s’agit d’une suite de la DLA-1942-1.</p>

<p>Il existait une certaine confusion à propos du correctif correct pour
 <a href="https://security-tracker.debian.org/tracker/CVE-2019-13776">CVE-2019-13776</a>.</p>

<p>L’annonce correcte pour cette DLA aurait due être :</p>

<p>Package : phpbb3
Version : 3.0.12-5+deb8u4
CVE ID: <a href="https://security-tracker.debian.org/tracker/CVE-2019-13776">CVE-2019-13776</a> <a href="https://security-tracker.debian.org/tracker/CVE-2019-16993">CVE-2019-16993</a></p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16993">CVE-2019-16993</a>

<p>Dans phpBB, includes/acp/acp_bbcodes.php vérifiait de manière incorrecte
le jeton CSRF dans la page des BBCode dans Administration Control Panel. Une attaque
CSRF réelle était possible si un attaquant réussissait à récupérer l’identifiant
de session d’un administrateur réauthentifié avant de les cibler.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13776">CVE-2019-13776</a>

<p>phpBB permettait le vol de l’identifiant de session du Administration Control
Panel en exploitant un CSRF dans la fonction Remote Avatar. Le vol du jeton CSRF
aboutit à un XSS stocké.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.0.12-5+deb8u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets phpbb3.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1942-2.data"
# $Id: $
