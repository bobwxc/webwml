#use wml::debian::translation-check translation="32edfbb76713c429c538b2db5049e03c559c04cf" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Des vulnérabilités ont été identifiées dans libspring-java, un cadriciel
d’application Java/J2EE modulaire.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-3578">CVE-2014-3578</a>

<p>Une vulnérabilité de traversée de répertoires permet à des attaquants distants
de lire des fichiers arbitraires à l'aide d'une URL contrefaite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-3625">CVE-2014-3625</a>

<p>Une vulnérabilité de traversée de répertoires permet à des attaquants distants
de lire des fichiers arbitraires à l’aide de vecteurs non précisés, relatifs au
traitement de ressources statiques.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-3192">CVE-2015-3192</a>

<p>Un traitement incorrect de déclarations DTD inline lorsque DTD n’est pas
totalement désactivé permet à des attaquants distants de provoquer un déni
de service (consommation de mémoire et erreurs d’épuisement de mémoire) à l'aide
d'un fichier XML contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-5211">CVE-2015-5211</a>

<p>Une vulnérabilité d’attaque par téléchargement de fichier par réflexion (RFD)
permet à un utilisateur malveillant de contrefaire une URL avec une extension
de script batch qui peut aboutir à ce que la réponse est téléchargée plutôt que
rendue et inclut aussi un peu d’entrée réfléchie dans la réponse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9878">CVE-2016-9878</a>

<p>Une vérification incorrecte dans ResourceServlet permet des attaques par
traversée de répertoires.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.0.6.RELEASE-17+deb8u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets libspring-java.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1853.data"
# $Id: $
