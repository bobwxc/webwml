#use wml::debian::template title="Debian GNU/kFreeBSD"

#use wml::debian::toc
#use wml::debian::translation-check translation="6f3adf6374f35194686f89dec2ba66b1ecf3bb5f"
# $Id$
# Translator: Florian Ernst <florian@debian.org>, 2004-09-02
# Translator: Axel Beckert <abe@debian.org>, 2010-02-28
# Updated: Holger Wansing <linux@wansing-online.de>, 2011
# Updated: Holger Wansing <hwansing@mailbox.org>, 2019.

<toc-display/>

<p>Die Portierung Debian GNU/kFreeBSD kombiniert einen 
<a href="https://www.freebsd.org/">FreeBSD</a>-Kernel, die 
<a href="https://www.gnu.org/">GNU-Benutzeranwendungen</a> unter Verwendung der
<a href="https://www.gnu.org/software/libc/">GNU C-Bibliothek</a> und den 
regulären <a href="https://packages.debian.org/">Satz an Debian-Paketen</a>.</p>

<div class="important">
<p>Debian GNU/kFreeBSD ist eine nicht offiziell unterstützte Architektur.
Sie wurde mit Debian 6.0 (Squeeze) und 7.0 (Wheezy) als <em>Technologievorschau</em>
veröffentlicht und ist die erste nicht-Linux-Portierung.
Seit Debian 8 (Jessie) ist allerdings nicht mehr Teil der offiziellen
Veröffentlichungen.</p>
</div>

<toc-add-entry name="resources">Ressourcen</toc-add-entry>

<p>Weitere Informationen über die Portierung (inklusive einer FAQ) finden Sie im
Debian Wiki auf der
<a href="https://wiki.debian.org/Debian_GNU/kFreeBSD">Debian-GNU/kFreeBSD</a>-Seite.
</p>

<h3>Mailinglisten</h3>
<p><a href="https://lists.debian.org/debian-bsd">Debian GNU/k*BSD-Mailingliste</a>.</p>

<h3>IRC</h3>
<p><a href="irc://irc.debian.org/#debian-kbsd">#debian-kbsd IRC-Kanal</a> (auf
irc.debian.org).</p>

<toc-add-entry name="Development">Entwicklung</toc-add-entry>

<p>Da wir
die Glibc verwenden, sind die Portierungsprobleme sehr klein.
Zumeist reicht es aus, einen Testfall für <q>k*bsd*-gnu</q> von einem anderen
Glibc-basierten System (wie GNU oder GNU/Linux) zu kopieren. Schauen Sie sich
das 
<a href="https://salsa.debian.org/bsd-team/web/raw/master/porting/PORTING">PORTING</a>-Dokument
hinsichtlich weiterer Details an.</p>

<p>Lesen Sie außerdem die
<a href="https://salsa.debian.org/bsd-team/web/raw/master/TODO">TODO</a>-Datei
bezüglich zusätzlicher Hinweise zu noch ausstehenden Arbeiten.</p>

<toc-add-entry name="availablehw">Für Debian-Entwickler verfügbare Hardware</toc-add-entry>

<p>lemon.debian.net (kfreebsd-amd64) ist
   für Debian-Entwickler für Portierungsarbeiten verfügbar. Bitte schauen Sie
   in die <a href="https://db.debian.org/machines.cgi">Rechner-Datenbank</a>
   für weitere Informationen über diese Maschinen. Im Allgemeinen können Sie
   zwei Chroot-Umgebungen benutzen: Testing und Unstable. Beachten Sie, dass
   diese Maschinen nicht von den <acronym lang="en" 
   title="Debian System Administrator">DSA</acronym>s administriert werden,
   <b>senden Sie daher keine Anfragen bezüglich dieser Maschinen an 
   debian-admin</b>. Verwenden Sie stattdessen
   <email "admin@lemon.debian.net">.</p>
