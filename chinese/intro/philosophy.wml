#use wml::debian::template title="我们的哲学：为何以及如何"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="8d625100deb403dba223e3cecbac44a283fe02ff"

# translators: some text is taken from /intro/about.wml

<ul class="toc">
<li><a href="#what">什么是 Debian？</a>
<li><a href="#free">全部都是自由的吗？</a>
<li><a href="#how">社区如何运作？</a>
<li><a href="#history">一切从何开始？</a>
</ul>

<h2><a name="what">什么是 Debian？</a></h2>

<p><a href="$(HOME)/">Debian 项目</a>是一个由拥有共同理念的个人组成的协会，
其目的是创建一个<a href="free">自由</a>的操作系统。这个由我们创造的操作系统叫做 
<strong>Debian</strong>。

<p>操作系统是使您的计算机得以运行的一组基本程序和实用程序。
操作系统的核心是内核。
内核是计算机上的基础程序，它完成了所有的基础任务并允许您启动其他程序。

<p>Debian 系统目前使用 <a href="https://www.kernel.org/">Linux</a>
 内核或 <a href="https://www.freebsd.org/">FreeBSD</a>
 内核。Linux 由
 <a href="https://en.wikipedia.org/wiki/Linus_Torvalds">Linus Torvalds</a>
创始，并得到了全球成千上万程序员的支持与维护。
FreeBSD 是一个包含了内核与其他软件的操作系统。

<p>同时，提供使用其他内核的 Debian 的工作也正在进行，
主要是 
<a href="https://www.gnu.org/software/hurd/hurd.html">Hurd</a>。
Hurd 是在微内核（如 Mach）上运行的用于实现不同的功能的服务集合。
Hurd 是由<a href="https://www.gnu.org/">GNU 项目</a>产生的自由软件。

<p>构成操作系统的许多基本工具
来自 <a href="https://www.gnu.org/">GNU 项目</a>；因此得名：
GNU/Linux，GNU/kFreeBSD，和 GNU/Hurd。
这些工具也同样是自由软件。

<p>当然了，人们想要的是应用软件——帮助他们完成他们想做的事情的程序，从编辑
文档到经营企业，从玩游戏到编写更多的软件。Debian 提供了
超过 <packages_in_stable> 个<a href="$(DISTRIB)/packages">软件包</a>（预编
译的软件被以一种良好的格式打包以轻松地安装到您的计算机上），一个软件包
管理器（APT），和其他实用程序，使您能够像安装单个应用程序一样轻松地管理
数千台计算机上的数千个软件包。所有这一切都是<a href="free">自由</a>的。

<p>这种结构类似于塔状。最下层是内核，
再上一层是所有的基本工具，
接下来是你在电脑上运行的所有软件；
塔顶上，由 Debian —— 
精心组织和安排所有的一切，以使它们协同工作。

<h2>全部都是<a href="free" name="free">自由</a>的吗？</h2>

<p>当我们使用“free”这个词的时候，我们想表达的其实是软件的
<strong>自由</strong>（区别于 免费）。你可以在
<a href="free">what we mean by "free software"</a> 和 
<a href="https://www.gnu.org/philosophy/free-sw">what the Free Software
Foundation says</a> 读到更多有关此主题的内容。

<p>你可能想知道：是什么让人们愿意花费他们自己的时间去编写
软件，仔细地打包之，然后把它们都直接<EM>送</EM>人呢？
问题的答案和我们的贡献者一样千差万别。
有些人喜欢帮助别人；
许多人编写程序来学习更多关于计算机的知识；
越来越多的人在寻找避免软件价格虚高的方法；
而对从别人那里得到的伟大的自由软件表示感谢的人也与日俱增。
还有许多学术界人士创建了免费软件，以帮助他们的研究成果得到更广泛的应用。
企业也帮助维护自由软件，这样他们就可以对软件的开发有发言权——
没有比自己实现新功能更快的方法了！
当然，我们很多人只是觉得很有趣。

<p>Debian 作出了自由软件的郑重承诺，我们认为需要将这种承诺形式化为一份书面
文件。于是，我们的
<a href="$(HOME)/social_contract">社群契约</a>诞生了。

<p>尽管 Debian 信仰自由软件，但在某些情况下，人们希望或需要在他们的机器上
安装非自由软件。只要有可能，Debian 就会支持这一点。甚至有越来越多的软件包
的唯一功能就是将非自由软件安装到 Debian 系统中。

<h2><a name="how">社区如何运作？</a></h2>

<p>Debian 是由分布在<a href="$(DEVEL)/developers.loc">世界各地</a>的近千名
活跃开发人员在业余时间自愿开发的。
很少有开发者真的见过面。
通信主要通过电子邮件（邮件列表参见 lists.debian.org）
和 IRC（irc.debian.org 的 #debian 频道）进行。
</p>

<p>Debian 项目拥有一个精心组织的<a href="organization">组织架构</a>。
有关 Debian 内部的更多信息，请自由浏览
<a href="$(DEVEL)/">开发者之家</a>。</p>

<p>
关于解释社区如何运作的主要文档如下：
<ul>
<li><a href="$(DEVEL)/constitution">Debian 宪章</a></li>
<li><a href="../social_contract">社群契约与自由软件指导方针</a></li>
<li><a href="diversity">多样性声明</a></li>
<li><a href="../code_of_conduct">行为准则</a></li>
<li><a href="../doc/developers-reference/">开发者参考</a></li>
<li><a href="../doc/debian-policy/">Debian 政策</a></li>
</ul>

<h2><a name="history">一切从何开始？</a></h2>

<p>Debian 于1993年8月由 Ian Murdock 创建，作为一个新的发行版，它遵循 Linux 和
 GNU 的精神公开发布。Debian 的本意是要将软件小心而认真地组合在一起，并以
类似的方式进行系统维护和支持。它最初是一个由自由软件极客组成的小团体，后
来逐渐发展成为一个由开发人员和用户组成的组织良好的大型社区。查看
<a href="$(DOC)/manuals/project-history/">详细历史记录</a>。

<p>由于很多人都问过（这里解答一下），Debian的发音是 /&#712;de.bi.&#601;n/。它
来自于 Debian 的创造者 Ian Murdock 和他的妻子 Debra 的名字。
